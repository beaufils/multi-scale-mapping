# Multi-Scale Mapping

This algorithm allows mapping Greenhouse Gases (GHG) emission data from the PRIMAP databases to Multi-Regional Input-Output Tables (MRIOTs).

## Overview

The method is based on the construction of so-called emission trees (nested emission categories) for each individual countries, based on two different data bases:
	- A database with high sectoral resolution but a low spatial resolution (PRIMAP-crf)
	- A database with low sectoral resolution but a high spatial resolution (PRIMAP-hist)
In the first step of the algorithm, high resolution sectoral data are combined with national output data at the sector scale to build an estimated emission tree for each country, using a bottom-up procedure.
Second, aggregated data available at high regional resolution are used to reconcile the emission trees with a top-down procedure.
The final emission tree is finally mapped to the MRIOT.

## Data Sources

The current implementation of the method relies on the [PRIMAP-hist](https://zenodo.org/record/7585420#.Y_eboR_MI2w) and [PRIMAP-crf](https://zenodo.org/record/4723476#.Y_ebwB_MI2z) databases.
The mapping is available for the three following input output tables:
	- [EORA](https://worldmrio.com/eora26/)
	- [WIOD](https://www.rug.nl/ggdc/valuechain/wiod/wiod-2016-release)
	- [ICIO](https://www.oecd.org/sti/ind/inter-country-input-output-tables.htm)
Mapping tables describing the relations between the MRIO sectors and the IPCC emission categories are provided in the mapping folder.

## Use

External data sources (PRIMAP; MRIO tables) are not provided in this repository. 
You should download them separately and indicate their location into the header of the script.
Example commands are provided into the Python script.

## Citation 

An initial version of the method has been published in the supplementary information of the following paper: Beaufils, T., Berthet, E., Ward, H., & Wenz, L. (2023). Beyond production and consumption: Using throughflows to untangle the virtual trade of externalities. Economic Systems Research. [https://doi.org/10.1080/09535314.2023.2174003](https://doi.org/10.1080/09535314.2023.2174003)
The description provided in this repository is an extension of this method description.

## Development

The following extensions are currently envisioned:
	- Extension to other externalities
	- Inclusion to additional allocation proxies (only sectoral outputs and average emission intensities are used in the current implementation).
